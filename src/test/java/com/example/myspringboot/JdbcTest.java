package com.example.myspringboot;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcTest {
    @SneakyThrows
    @Test
    public void testConn(){
        Class.forName("com.mysql.cj.jdbc.Driver");
        var url = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&characterEncoding=utf8&serverTimezone=GMT";
        var user = "root";
        var password = "123456";
        Connection conn = DriverManager.getConnection(url,user,password);
        @Cleanup var ps = conn.prepareStatement("select * from test;");
        @Cleanup var rs = ps.executeQuery();
        System.out.println(rs.next());
    }
}
