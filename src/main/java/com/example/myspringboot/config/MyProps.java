package com.example.myspringboot.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置注入pojo
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@ConfigurationProperties(prefix = "my")
public class MyProps {
    private String name;
    private String habit;
}
