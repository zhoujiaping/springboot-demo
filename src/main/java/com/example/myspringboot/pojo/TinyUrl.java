package com.example.myspringboot.pojo;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TinyUrl {
    private Long tinyUrlId;
    private String tinyUrl;
    private String longUrl;
}
