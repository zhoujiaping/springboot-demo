package com.example.myspringboot.jdbc;

import java.util.List;

public interface BaseDao<PK,T> {
    default void init(String tableName, String pkColumnName){};
    T query(PK id);
    List<T> query();
    int insertSelective(T t);
    int insertSelectiveWithPk(T t);
    int insertSelectiveWithAutoGenPk(T t);
    int updateSelective(T t);
    int updateSelectiveWithOptimisticLock(T t);
    int batchInsert(List<T> list);
    int insertOrUpdateSelective(T t);
}
