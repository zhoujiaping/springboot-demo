package com.example.myspringboot.jdbc;

import com.example.myspringboot.util.SqlUtils;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 开发者自定义的dao，只需要继承jdbcDao，就能获得基本的crud功能。
 * 默认
 * 表名为  "t_" + 实体类名驼峰转下划线。
 * 主键名为 实体类名驼峰转下划线 + “_id”
 * ps：如果不符合该规则，子类可以通过覆盖init方法自己设置相关元数据
 * 默认乐观锁版本号字段名为 "version"
 * 不支持联合主键
 *
 *
 *
 */
public abstract class AbstractJdbcDao<PK, T> extends NamedParameterJdbcDaoSupport implements BaseDao<PK, T> {
    protected Class<PK> pkClazz;
    protected Class<T> modelClazz;
    protected String versionColumnName = "version";
    protected Set<String> columnNames;
    protected Set<String> columnNamesNoPk;

    protected String tableName;
    protected String pkColumnName;
    protected String pkPropName;

    protected RowMapper<T> rowMapper;

    protected Map<String, Field> columnNameToFieldMap = new TreeMap();

    public AbstractJdbcDao() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        pkClazz = (Class<PK>) type.getActualTypeArguments()[0];
        modelClazz = (Class<T>) type.getActualTypeArguments()[1];
        var uncapitalizedName = StringUtils.uncapitalize(modelClazz.getSimpleName());
        var underscoredName = uncapitalizedName.replaceAll("(?<Uper>[A-Z])", "_${Uper}").toLowerCase();
        var tName = "t_" + underscoredName;
        var pkName = underscoredName + "_id";
        init(tName, pkName);
    }

    @Override
    public void init(String tableName, String pkColumnName) {
        this.tableName = tableName;
        this.pkColumnName = pkColumnName;
        this.pkPropName = JdbcUtils.convertUnderscoreNameToPropertyName(pkColumnName);
        rowMapper = new BeanPropertyRowMapper(modelClazz);
        ReflectionUtils.doWithFields(modelClazz, it -> {
            if (!Modifier.isStatic(it.getModifiers()) && !it.getName().equals("metaClass")) {
                var columnName = it.getName().replaceAll("(?<Uper>[A-Z])", "_${Uper}").toLowerCase();
                columnNameToFieldMap.put(columnName, it);
            }
        });
        columnNames = columnNameToFieldMap.keySet();
        columnNamesNoPk = columnNames.stream().filter(it -> !it.equals(pkColumnName)).collect(Collectors.toSet());
    }

    @Override
    public T query(PK id) {
        var sql = SqlUtils.concat("select * from", tableName, "where", pkColumnName, "=:pk");
        return (T) getNamedParameterJdbcTemplate().queryForObject(sql, Map.of("pk", id), new BeanPropertyRowMapper(modelClazz));
    }

    @Override
    public List<T> query() {
        var sql = SqlUtils.concat("select * from", tableName);
        return getNamedParameterJdbcTemplate().query(sql, Map.of(), new BeanPropertyRowMapper(modelClazz));
    }

    /**
     * 如果传了主键，就用传入的主键。否则，用自增长的主键。
     *
     * @param t
     */
    @Override
    public int insertSelective(T t) {
        var acc = new DirectFieldAccessor(t);
        if (acc.getPropertyValue(pkPropName) == null) {
            return insertSelectiveWithAutoGenPkInternal(acc, t);
        } else {
            return insertSelectiveWithPkInternal(acc, t);
        }
    }

    @Override
    public int insertSelectiveWithPk(T t) {
        var acc = new DirectFieldAccessor(t);
        return insertSelectiveWithPkInternal(acc, t);
    }

    @Override
    public int insertSelectiveWithAutoGenPk(T t) {
        var acc = new DirectFieldAccessor(t);
        return insertSelectiveWithAutoGenPkInternal(acc, t);
    }

    private int insertSelectiveWithPkInternal(DirectFieldAccessor acc, T t) {
        var columnNamesSelective = columnNames.stream().filter(it ->
                acc.getPropertyValue(columnNameToFieldMap.get(it).getName()) != null
        ).collect(Collectors.toList());
        var columns = columnNamesSelective.stream().collect(Collectors.joining(","));
        var values = columnNamesSelective.stream().map(it ->
                ":" + columnNameToFieldMap.get(it).getName()).collect(Collectors.joining(",")
        );
        var sql = SqlUtils.concat("insert into", tableName, "(", columns, ")values(", values, ")");
        return getNamedParameterJdbcTemplate().update(sql,
                new BeanPropertySqlParameterSource(t));
    }

    private int insertSelectiveWithAutoGenPkInternal(DirectFieldAccessor acc, T t) {
        var columnNamesSelective = columnNamesNoPk.stream().filter(it ->
                acc.getPropertyValue(columnNameToFieldMap.get(it).getName()) != null
        ).collect(Collectors.toList());
        var columns = columnNamesSelective.stream().collect(Collectors.joining(","));
        var values = columnNamesSelective.stream().map(it ->
                ":" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.joining(","));
        KeyHolder kh = new GeneratedKeyHolder();
        var sql = SqlUtils.concat("insert into", tableName, "(", columns, ")values(", values, ")");
        var count = getNamedParameterJdbcTemplate().update(sql,
                new BeanPropertySqlParameterSource(t), kh, new String[]{pkColumnName});
        acc.setPropertyValue(pkPropName, kh.getKey());
        return count;
    }

    @Override
    public int updateSelective(T t) {
        var acc = new DirectFieldAccessor(t);
        var columnNamesSelective = columnNamesNoPk.stream().filter(it ->
                acc.getPropertyValue(columnNameToFieldMap.get(it).getName()) != null
        ).collect(Collectors.toList());
        var sets = columnNamesSelective.stream().map(it ->
                it + "=:" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.joining(","));
        var sql = SqlUtils.concat("update", tableName, "set", sets, "where", pkColumnName, "=:" + pkPropName);
        return getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(t));
    }

    @Override
    public int updateSelectiveWithOptimisticLock(T t) {
        var acc = new DirectFieldAccessor(t);
        var columnNamesSelective = columnNamesNoPk.stream().filter(it ->
                it != versionColumnName && acc.getPropertyValue(columnNameToFieldMap.get(it).getName()) != null
        ).collect(Collectors.toList());
        var setList = columnNamesSelective.stream().map(it ->
                it + "=:" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.toList());
        setList.add(versionColumnName + "=" + versionColumnName + "+1");
        var sets = setList.stream().collect(Collectors.joining(","));
        var sql = SqlUtils.concat("update", tableName, "set", sets, "where", pkColumnName, "=:" + pkPropName);
        return getNamedParameterJdbcTemplate().update(sql,
                new BeanPropertySqlParameterSource(t));
    }

    @Override
    public int batchInsert(List<T> list) {
        var columns = columnNamesNoPk.stream().collect(Collectors.joining(","));
        var values = columnNamesNoPk.stream().map(it ->
                ":" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.joining(","));
        var sources = list.stream().map(it ->
                new BeanPropertySqlParameterSource(it)
        ).collect(Collectors.toList());
        var sql = SqlUtils.concat("insert into ", tableName, columns, "values (", values, ")");
        int[] counts = getNamedParameterJdbcTemplate().batchUpdate(sql, sources.toArray(new BeanPropertySqlParameterSource[]{}));
        return IntStream.of(counts).sum();
    }

    @Override
    public int insertOrUpdateSelective(T t) {
        var acc = new DirectFieldAccessor(t);
        var columnNamesSelective = columnNames.stream().filter(it ->
                it != versionColumnName && acc.getPropertyValue(columnNameToFieldMap.get(it).getName()) != null
        ).collect(Collectors.toList());
        var columns = columnNamesSelective.stream().collect(Collectors.joining(","));
        var values = columnNamesSelective.stream().map(it ->
                ":" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.joining(","));
        var updates = columnNamesSelective.stream().map(it ->
                it + "=:" + columnNameToFieldMap.get(it).getName()
        ).collect(Collectors.toList());
        updates.add(versionColumnName + "=" + versionColumnName + "+1");
        var updatesPart = updates.stream().collect(Collectors.joining(","));
        var sql = SqlUtils.concat("insert into ", tableName, "(", columns, ") values (", values, ") ON DUPLICATE KEY UPDATE ", updatesPart);
        return getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(t));
    }
}
