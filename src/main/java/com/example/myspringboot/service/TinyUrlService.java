package com.example.myspringboot.service;

import com.example.myspringboot.jdbc.TinyUrlDao;
import com.example.myspringboot.pojo.TinyUrl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class TinyUrlService {
    @Autowired
    TinyUrlDao tinyUrlDao;
    @Transactional
    //@Cacheable(value="tinyUrl",key="#qry")
    @Cacheable(value="tinyUrl")
    public List<TinyUrl> query() {
        /*tinyUrlDao.insertSelective(TinyUrl.builder()
                .tinyUrl("1").longUrl("http:localhost:8080").build());*/
        return tinyUrlDao.query();
    }
}
