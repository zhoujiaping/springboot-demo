package com.example.myspringboot.controller;

import com.example.myspringboot.jdbc.TinyUrlDao;
import com.example.myspringboot.pojo.TinyUrl;
import com.example.myspringboot.service.TinyUrlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.constraints.Max;
import java.util.List;

@RestController
@RequestMapping("/hello")
@Slf4j
@Validated
public class HelloController {
    @Autowired
    TinyUrlService tinyUrlService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @GetMapping
    public Mono<String> hello(){

        //if(1==1)throw new RuntimeException("i am a exception!");
        return Mono.just("hello world");
        //return Map.of("hello","world");
    }
    @GetMapping("/tinyUrl")
    public Mono<List<TinyUrl>> tinyUrl(){
        return Mono.just(tinyUrlService.query());
    }

    @GetMapping("/redis")
    public Mono<String> pingRedis(String key){
        return Mono.justOrEmpty(stringRedisTemplate.opsForValue().get(key));
    }

    @GetMapping("/valid")
    public Mono<String> validate(@Max(100) int magic){
        return Mono.just("validation ok!");
    }
}
