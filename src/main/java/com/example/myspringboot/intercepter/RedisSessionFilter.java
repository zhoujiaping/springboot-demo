package com.example.myspringboot.intercepter;

import com.example.myspringboot.util.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.server.PathContainer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import org.springframework.web.util.pattern.PathPattern;
import org.springframework.web.util.pattern.PathPatternParser;
import reactor.core.publisher.Mono;

import java.util.Map;

/**redis保存会话*/
//拦截登录失效的请求
@Component
public class RedisSessionFilter implements WebFilter {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        /*serverWebExchange.getSession().subscribe(session->{
            String key = session.getId();
            String json = stringRedisTemplate.opsForValue().get(key);
            if(json==null){
                throw new RuntimeException("未登陆");
            }
        });*/
        PathPattern pattern =PathPatternParser.defaultInstance.parse(serverWebExchange.getRequest().getURI().getRawPath());
        if(pattern.matches(PathContainer.parsePath("/login"))){
            return webFilterChain.filter(serverWebExchange);
        }
        return serverWebExchange.getSession().flatMap(session->{
            String key = session.getId();
            String json = stringRedisTemplate.opsForValue().get(key);
            if(json==null){
                ServerHttpResponse response = serverWebExchange.getResponse();
                byte[] bits = JSON.stringify(Map.of("code","401","msg","会话过期"))
                        .getBytes();
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                return response.writeWith(Mono.just(buffer));
                //throw new RuntimeException("未登陆");
            }
            return webFilterChain.filter(serverWebExchange);
        });
        //return webFilterChain.filter(serverWebExchange);
    }
}