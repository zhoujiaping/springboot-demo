package com.example.myspringboot;

import com.example.myspringboot.listener.MyListener;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableCaching
public class MyspringbootApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplicationBuilder()
				.sources(MyspringbootApplication.class)
				//.child(Application.class)
				.bannerMode(Banner.Mode.CONSOLE)
				.build();
		//如果同时依赖了spring-boot-starter-web 和spring-boot-starter-webflux，默认使用springmvc，
		// 需要强制设置web应用类型才能使用webflux
		app.setWebApplicationType(WebApplicationType.REACTIVE);
		app.addListeners(new MyListener(),new ApplicationPidFileWriter("/etc/myspringboot/app.pid"));
		app.setApplicationStartup(new BufferingApplicationStartup(2048));
		app.run(args);
		//SpringApplication.run(MyspringbootApplication.class, args);
	}

}
