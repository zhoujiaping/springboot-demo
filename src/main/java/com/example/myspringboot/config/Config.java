package com.example.myspringboot.config;

import com.alibaba.druid.pool.DruidDataSource;
import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.admin.SpringApplicationAdminMXBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.resources.LoopResources;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.time.Duration;

@Component
@Configuration
@Slf4j
public class Config {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    //@Autowired
    SpringApplicationAdminMXBean springApplicationAdminMXBean;
    @PostConstruct
    public void init(){
        //String port = springApplicationAdminMXBean.getProperty("local.server.port");
        //springApplicationAdminMXBean.shutdown();
        //springApplicationAdminMXBean.isEmbeddedWebApplication();
        //logger.info("server running at port:{}",port);
    }

    /**使用druid数据源*/
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource(){
        return new DruidDataSource();
    }

    /**webclient配置*/
    @Bean
    public WebClientCustomizer webClientCustomizer(){
        //配置动态连接池
        //ConnectionProvider provider = ConnectionProvider.elastic("elastic pool");
        //配置固定大小连接池，如最大连接数、连接获取超时、空闲连接死亡时间等
        var provider = ConnectionProvider.builder("fixed").maxConnections(45)
                .evictInBackground(Duration.ofSeconds(20))
                .maxIdleTime(Duration.ofSeconds(6))
                .pendingAcquireTimeout(Duration.ofSeconds(4))
                .pendingAcquireMaxCount(100)
                .maxLifeTime(Duration.ofSeconds(60))
                .build();
        //ConnectionProvider provider = ConnectionProvider.fixed("fixed", 45, 4000, Duration.ofSeconds(6));
        LoopResources loop = LoopResources.create("kl-event-loop", 1, 4, true);
        HttpClient httpClient = HttpClient.create(provider)
                /* .secure(sslContextSpec -> {
                     SslContextBuilder sslContextBuilder = SslContextBuilder.forClient()
                             .trustManager(new File("E://server.truststore"));
                     sslContextSpec.sslContext(sslContextBuilder);
                 })*/
                .keepAlive(true)
                .responseTimeout(Duration.ofSeconds(10))
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
                .option(ChannelOption.TCP_NODELAY, true)
                .runOn(loop);
        return webClientBuilder-> {
            log.info("webClientBuilder");
            webClientBuilder.clientConnector(new ReactorClientHttpConnector(httpClient));
        };
    }

}